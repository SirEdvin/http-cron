#!/usr/bin/env python3
import click

from http_cron.app import app

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, KITWAY"
__credits__ = ["KITWAY"]
__license__ = "Proprietary"
__version__ = "0.6.1"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "b.gladyshev@kitway.com.ua"
__status__ = "Production"


@click.group()
def cli():
    pass


@cli.command()
@click.option('--host', default='0.0.0.0')
@click.option('--port', default=8077, type=int)
@click.option('--debug', default=False, is_flag=True)
@click.pass_context
def run(ctx, host, port, debug):
    app.run(host=host, port=port, debug=debug)


# @cli.group()
# def gendata():
#     pass


# @gendata.command()
# @click.option('--count', default=5, type=int)
# def orders(count):
#     with open('fakedata.json') as fakedata_file:
#         fakedata = ujson.load(fakedata_file)
#     for _ in range(0, count):
#         order_template = fakedata['order_template']
#         order_template['Orders'][0]['ShipmentID'] = str(int(uuid.uuid4()))
#         click.echo(f"Create order with shipment {order_template['Orders'][0]['ShipmentID']}")
#         requests.post('http://127.0.0.1:9602/MFY/SendOrder.svc/SendOrders', json=order_template)


if __name__ == "__main__":
    cli()
