from sanic import Blueprint
from sanic.request import Request
from sanic.response import HTTPResponse, json
from sanic_openapi import doc

from ..models import Worker
from ..utils import Message

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"

worker_api = Blueprint('Worker api', url_prefix='/api')  # pylint: disable=invalid-name


@worker_api.post('/workers/expire')
@doc.summary("Expire old workes and free tasks")
@doc.route(produces=Message)
async def check_worker_expire(_request: Request) -> HTTPResponse:
    await Worker.expire_workers()
    return json({"message": "workers was cleaned"})
