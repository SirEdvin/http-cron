import asyncio
import logging

from sanic import Blueprint
from sanic.request import Request
from sanic.response import HTTPResponse, json
from sanic_openapi import doc
from websockets import WebSocketClientProtocol

from ..models import HTTPCronTask
from ..utils import Message

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
_log = logging.getLogger(__name__)

task_api = Blueprint("Task api", url_prefix='/api')


@task_api.post('/task')
@doc.summary("Creates a cron http task")
@doc.consumes(HTTPCronTask.get_description(), location='body')
@doc.route(produces=Message)
async def create_task(request: Request) -> HTTPResponse:
    create_result = await HTTPCronTask.new_from_post(request.json)
    if create_result:
        return json({"message": "task was created"})
    return json({"message": "task already exists"})


@task_api.patch('/task/<task_id>')
@doc.summary("Updates a cron http task")
@doc.consumes(HTTPCronTask.get_description(), location='body')
@doc.route(produces=Message)
async def update_task(request: Request, task_id: str) -> HTTPResponse:
    target_task: HTTPCronTask = await HTTPCronTask.async_get(task_id)
    if not target_task:
        return json({"message": "Task not found"}, status=404)
    await target_task.async_update(request.json)
    return json({"message": "Task was updated"})


@task_api.delete('/task/<task_id>')
@doc.summary("Updates a cron http task")
@doc.route(produces=Message)
async def delete_task(_request: Request, task_id: str) -> HTTPResponse:
    target_task: HTTPCronTask = await HTTPCronTask.async_get(task_id)
    if not target_task:
        return json({"message": "Task not found"}, status=404)
    await target_task.async_delete()
    return json({"message": "Task was deleted"})


@task_api.get('/tasks')
@doc.summary("List of cron http tasks")
@doc.consumes(
    {"filter": doc.String("Filter for rethinkdb match expression")},
    {"pageIndex": doc.String("Index of current page")},
    {"sortDirection": doc.String("Direction for sorting", choices=('desc', 'asc'))},
    {"pageSize": doc.Integer("Size for page")},
    {"sortColumn": doc.String("Column for sorting")}
)
@doc.route(produces=[HTTPCronTask.get_full_description()])
async def get_tasks(request: Request) -> HTTPResponse:
    return await HTTPCronTask.process_web_request(request, prettify_response=False)


@task_api.websocket('/task/feed')
async def feed(request: Request, websocket: WebSocketClientProtocol) -> None:
    request.app.websocket_storage.add(websocket)
    try:
        while websocket.open:
            await websocket.ping()
            await asyncio.sleep(60)
    finally:
        request.app.websocket_storage.remove(websocket)
