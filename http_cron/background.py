import asyncio
from asyncio import AbstractEventLoop, CancelledError
import logging
from typing import Dict, Any

from sanic import Blueprint, Sanic
from anji_orm import register
import ujson

from .models import HTTPCronTask, Worker
from .utils import WORKER_HEALTH_CHECK_PERIOR

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
_log = logging.getLogger(__name__)


background_blueprint = Blueprint("Http cron configuration")


async def task_execution_loop(sanic_app: Sanic):
    try:
        while True:
            new_tasks = await HTTPCronTask.get_new_tasks(1)
            if new_tasks:
                for task in new_tasks:
                    if await task.book(sanic_app.worker.id):
                        _log.info("Run task to execute query to %s", task.url)
                        await task.run_task(sanic_app.async_session)
            await asyncio.sleep(30)
    except CancelledError as exc:
        # Process to avoid logging this exception
        raise
    except Exception as exc:
        _log.exception(exc)  # type: ignore
        raise


async def worker_healthcheck(sanic_app: Sanic):
    try:
        while True:
            await sanic_app.worker.health_report()
            await asyncio.sleep(WORKER_HEALTH_CHECK_PERIOR)
    except CancelledError:
        _log.info("Shutdown worker %s", sanic_app.worker.id)
        await sanic_app.worker.shutdown_worker()
        raise
    except Exception as exc:
        _log.info("Critical worker %s shutdown", sanic_app.worker.id)
        await sanic_app.worker.shutdown_worker()
        _log.exception(exc)  # type: ignore
        raise


async def subscribe_to_task_feed(sanic_app: Sanic):
    try:
        async with register.pool.connect() as conn:
            feed = await HTTPCronTask.all().changes(include_types=True).run(conn)
            while await feed.fetch_next():
                data = await feed.next()
                await sanic_app.broadcast_message({
                    "type": "rethinkdb_change",
                    "data": data
                })
    except CancelledError as exc:
        # Process to avoid logging this exception
        raise
    except Exception as exc:
        _log.exception('Error when listen task feed %s', exc)
        raise


@background_blueprint.listener("before_server_start")
async def basic_configuration(app: Sanic, _loop: AbstractEventLoop) -> None:
    app.websocket_storage = set()

    async def broadcast_message(message: Dict[str, Any]) -> None:
        try:
            dumped_message = ujson.dumps(message)  # pylint: disable=c-extension-no-member
            if app.websocket_storage:
                await asyncio.wait([
                    websocket.send(dumped_message) for websocket
                    in app.websocket_storage if websocket.open
                ])
            else:
                _log.info('Cannot broadcast message, no websocket connected')
                _log.debug('Message: %s', dumped_message)
        except Exception as exc:
            _log.error(str(exc))
            raise

    app.broadcast_message = broadcast_message


@background_blueprint.listener("after_server_start")
async def base_server_configuration(app: Sanic, loop: AbstractEventLoop) -> None:
    app.worker = Worker()
    await app.worker.async_send()
    app.tasks_list.append(loop.create_task(task_execution_loop(app)))
    app.tasks_list.append(loop.create_task(worker_healthcheck(app)))
    app.tasks_list.append(loop.create_task(subscribe_to_task_feed(app)))
