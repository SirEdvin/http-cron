from sanic_openapi import doc

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"

WORKER_HEALTH_CHECK_PERIOR = 30


class Message:  # pylint: disable=too-few-public-methods
    message = doc.String("Message")
