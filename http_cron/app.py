import os

from sanic import Sanic
from sanic.request import Request
from sanic.response import file_stream, HTTPResponse, json
from sanic.exceptions import NotFound

from sanic_prometheus import monitor
from sanic_openapi import swagger_blueprint, openapi_blueprint, doc
from sanic_service_utils.common import (
    anji_orm_configuration, aiohttp_session_configuration, log_configuration,
    sentry_configuration, background_task_configuration
)

from .api.task_api import task_api
from .api.worker_api import worker_api
from .background import background_blueprint

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"
STATIC_FOLDER_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static')

app = Sanic('http_cron')
app.blueprint(anji_orm_configuration)
app.blueprint(aiohttp_session_configuration)
app.blueprint(log_configuration)
app.blueprint(sentry_configuration)
app.blueprint(task_api)
app.blueprint(worker_api)
app.blueprint(swagger_blueprint)
app.blueprint(openapi_blueprint)
app.blueprint(background_blueprint)
app.blueprint(background_task_configuration)
monitor(app).expose_endpoint()

app.static('/', STATIC_FOLDER_PATH)


app.config.API_VERSION = __version__
app.config.API_TITLE = 'HTTP Cron API'
app.config.API_DESCRIPTION = 'HTTP Cron API'
app.config.API_PRODUCES_CONTENT_TYPES = ['application/json']
app.config.API_CONTACT_EMAIL = __email__


@app.route('/')
@app.exception(NotFound)
@doc.exclude(True)
async def redirect_to_index(request: Request, exception=None) -> HTTPResponse:  # pylint: disable=unused-argument
    return await file_stream(os.path.join(STATIC_FOLDER_PATH, 'index.html'))


@app.route('/about')
async def about(_request: Request) -> HTTPResponse:
    return json({
        'name': 'HTTP Cron',
        'version': __version__,
        'license': __license__
    })
