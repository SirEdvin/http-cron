from datetime import datetime, timedelta
from enum import Enum
import logging
from typing import List, Dict

from croniter import croniter
from aiohttp import ClientSession
from anji_orm import (
    Model, StringField, DatetimeField, EnumField,
    register, BooleanField, JsonField, DictField
)
from sanic_service_utils.orm_utils import DataTableMixin, OpenAPIDescriptionMixin
import rethinkdb as R

from .utils import WORKER_HEALTH_CHECK_PERIOR

__author__ = "Bogdan Gladyshev"
__copyright__ = "Copyright 2017, Bogdan Gladyshev"
__credits__ = ["Bogdan Gladyshev"]
__license__ = "MIT"
__version__ = "0.1.0"
__maintainer__ = "Bogdan Gladyshev"
__email__ = "siredvin.dark@gmail.com"
__status__ = "Production"

_log = logging.getLogger(__name__)


class TaskStatus(Enum):

    free = 'free'
    booked = 'booked'
    executing = 'executing'


class HTTPCronTask(OpenAPIDescriptionMixin, DataTableMixin):

    _table = 'http_cron_task'

    url = StringField(description='Url, that should be requsted (only get currently)')
    method = StringField(description='Request method', default='GET')
    request_body = JsonField(description='Request body', default=None)
    request_params = DictField(description='Request params', default=None)
    name = StringField(description='Unique name of task', secondary_index=True, definer=True)
    last_execution_time = DatetimeField(default=None, optional=True)
    next_execution_time = DatetimeField(optional=True)
    cron_expression = StringField(description='Cron expression for task')
    worker_id = StringField(optional=True)
    status = EnumField(TaskStatus, secondary_index=True, default=TaskStatus.free)
    is_active = BooleanField(default=True)

    @classmethod
    async def get_new_tasks(cls, task_count: int = 1) -> List['HTTPCronTask']:
        current_time = datetime.now(R.make_timezone("00:00"))
        return await register.async_execute(
            (
                cls.is_active & (cls.next_execution_time <= current_time) &
                (cls.status == TaskStatus.free)
            ).build_query().order_by('next_execution_time').limit(task_count)
        )

    @classmethod
    async def new_from_post(cls, json_data: Dict[str, str]) -> bool:
        """
        Try to create new task from post data. Create task only if this tasks not exists

        :returns: Is task was created?
        """
        name = json_data['name']
        url = json_data['url']
        cron_expression = json_data['cron_expression']
        # Unique check
        unique_query = (
            (HTTPCronTask.name == name) &
            (HTTPCronTask.url == url) &
            (HTTPCronTask.cron_expression == cron_expression)
        )
        if await register.async_execute(cls.count(unique_query)) > 0:
            return False
        new_cron_task = HTTPCronTask(
            name=name, cron_expression=cron_expression, url=url,
            next_execution_time=croniter(
                cron_expression, datetime.now(R.make_timezone("00:00"))
            ).get_next(datetime)
        )
        await new_cron_task.async_send()
        return True

    async def book(self, worker_id: str) -> bool:
        result = await register.async_execute(self.table.get(self.id).update(
            R.branch(
                R.row['status'] == TaskStatus.free.value,
                {"status": TaskStatus.booked.value},
                None
            )
        ))
        if not result.get("replaced"):
            _log.debug('Task %s already booked by another worker, skip.', self.id)
            return False
        self.worker_id = worker_id
        self.status = TaskStatus.booked
        await self.async_send()
        return True

    async def run_task(self, aiohttp_session: ClientSession) -> None:
        self.status = TaskStatus.executing
        await self.async_send()
        current_time = datetime.now(R.make_timezone("00:00"))
        response = await aiohttp_session.request(
            self.method, self.url,
            json=self.request_body, params=self.request_params
        )
        if response.status != 200:
            _log.exception(response)
        else:
            _log.debug(response)
        self.last_execution_time = current_time
        self.next_execution_time = croniter(
            self.cron_expression, current_time
        ).get_next(datetime)
        self.status = TaskStatus.free
        await self.async_send()


class Worker(Model):

    _table = 'http_cron_worker'

    is_active = BooleanField(default=True)
    last_health_report = DatetimeField()

    @classmethod
    async def expire_workers(cls) -> None:
        valid_health_report_time = (
            datetime.now(R.make_timezone("00:00")) +
            timedelta(seconds=WORKER_HEALTH_CHECK_PERIOR * 2)
        )
        old_workers = await (Worker.last_health_report <= valid_health_report_time).async_run()
        for old_worker in old_workers:
            await old_worker.shutdown_worker()

    async def shutdown_worker(self) -> None:
        update_query = (HTTPCronTask.worker_id == self.id).build_query().update(
            {
                'status': TaskStatus.free.value,
                'worker_id': None
            }
        )
        await register.async_execute(update_query)
        await self.async_delete()

    async def health_report(self) -> None:
        self.last_health_report = datetime.now(R.make_timezone("00:00"))
        await self.async_send()
