HTTP Cron
=========

Very simple Cron as a Service, that execute http requests in required time.

Installaition
-------------

You can run http cron via docker:

.. code:: bash

    docker run -d -p 8077:8077 registry.gitlab.com/siredvin/http-cron:v0.1.0


You install it to virtualenv via:

.. code:: bash

    git clone registry.gitlab.com/siredvin/http-cron:tag
    pip install -r requirements.txt
    gunicorn -w 2 -b 127.0.0.1:8077 --capture-output --worker-class sanic.worker.GunicornWorker --log-level INFO http_cron.app:app


Working with api
----------------

You can get API documentation at http://127.0.0.1:8077/swagger
