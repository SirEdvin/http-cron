check:
	pylint http_cron
	pycodestyle http_cron
	mypy http_cron --ignore-missing-imports
	restructuredtext-lint README.rst
