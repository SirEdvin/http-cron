import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CoreModule } from './@core/core.module';
import { RouterModule } from '@angular/router'; // we also need angular router for Nebular to function properly
import { HttpClientModule } from '@angular/common/http';

import { NbSidebarModule, NbLayoutModule, NbSidebarService, NbCardModule, NbThemeModule } from '@nebular/theme';
import { MomentModule } from 'ngx-moment';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ThemeModule } from './@theme/theme.module';
import { CronTaskCardComponent } from './cron-task-card/cron-task-card.component';
import { CronManagementComponent } from './cron-management/cron-management.component';


import { CronTaskService } from './cron-task.service'

@NgModule({
  declarations: [
    AppComponent,
    CronTaskCardComponent,
    CronManagementComponent
  ],
  imports: [
    BrowserModule,
    ThemeModule.forRoot(),
    CoreModule.forRoot(),
    NbThemeModule.forRoot({ name: 'default' }),
    NbCardModule,
    RouterModule,
    NbLayoutModule,
    NbSidebarModule,
    AppRoutingModule,
    HttpClientModule,
    MomentModule
  ],
  providers: [NbSidebarService, HttpClientModule, CronTaskService],
  bootstrap: [AppComponent]
})
export class AppModule { }
