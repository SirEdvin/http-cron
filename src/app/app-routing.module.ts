import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CronManagementComponent } from './cron-management/cron-management.component'

const routes: Routes = [
    { path: '', component: CronManagementComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
