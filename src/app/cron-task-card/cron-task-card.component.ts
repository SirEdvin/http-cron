import { Component, Input, OnInit } from '@angular/core';

import { HTTPCronTask } from '../models'
import { CronTaskService } from '../cron-task.service'


@Component({
  selector: 'app-cron-task-card',
  templateUrl: './cron-task-card.component.html',
  styleUrls: ['./cron-task-card.component.scss']
})
export class CronTaskCardComponent implements OnInit {

  @Input() cronTask: HTTPCronTask;
  @Input() type: string;

  constructor(private cronTaskService: CronTaskService) {
  }

  ngOnInit() { }

  toggleCronTask() {
      this.cronTaskService.patchTask(
          this.cronTask.id,
          {'is_active': !this.cronTask.is_active}
      ).subscribe(res => res);
  }

}
