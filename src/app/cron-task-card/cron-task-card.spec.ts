import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CronTaskCardComponent } from './cron-task-card.component';

describe('CronTaskCardComponent', () => {
  let component: CronTaskCardComponent;
  let fixture: ComponentFixture<CronTaskCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CronTaskCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CronTaskCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
