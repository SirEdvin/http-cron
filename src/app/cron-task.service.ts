import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { HTTPCronTask } from './models'

@Injectable()
export class CronTaskService {

  constructor(private http: HttpClient) { }

  getTaskList() {
      let params = new HttpParams().set('sortColumn', 'name');
      return this.http.get('/api/tasks', {params: params});
  }

  patchTask(task_id, task_data) {
      return this.http.patch(`/api/task/${task_id}`, task_data)
  }

}
