import { Component, OnInit } from '@angular/core';

import * as ReconnectingWebsocket from "reconnecting-websocket"

import { CronTaskService } from '../cron-task.service'
import { HTTPCronTask } from '../models'

@Component({
    selector: 'app-cron-management',
    templateUrl: './cron-management.component.html',
    styleUrls: ['./cron-management.component.css']
})
export class CronManagementComponent implements OnInit {

    cronTasks: [HTTPCronTask] | null;
    taskFeed: ReconnectingWebsocket;
    status: string;
    technicalStatus: string;

    constructor(private cronTaskService: CronTaskService) {
        let self = this;
        this.status = 'Processing ...'
        this.technicalStatus = 'None'
        this.taskFeed = new ReconnectingWebsocket(`ws://${window.location.host}/api/task/feed`);
        this.taskFeed.addEventListener('open', (evt) => {
            self.status = 'Connected to server!';
            this.technicalStatus = 'good';
            this.fetchCronTaskList();
        })
        this.taskFeed.addEventListener('close', (evt) => {
            self.status = 'Disconnected from server';
            this.technicalStatus = 'bad';
        })
        this.taskFeed.addEventListener('message', (evt) => {
            if (evt.type == 'message') {
                self.processTaskFeedMessage(JSON.parse(evt.data));
            }
        })
    }

    processTaskFeedMessage(message) {
        if (message.type == 'rethinkdb_change') {
            if (message.data.new_val == null) {
              // Process delete logic branch
              let cronTaskIndex = this.cronTasks.findIndex(
                  cronTask => cronTask.id == message.data.old_val['id']
              );
              if (cronTaskIndex != -1) {
                this.cronTasks.splice(cronTaskIndex, 1);
              }
            } else {
                let cronTaskIndex = this.cronTasks.findIndex(
                    cronTask => cronTask.id == message.data.new_val['id']
                );
                if (cronTaskIndex == -1) {
                    this.cronTasks.push(message.data.new_val);
                } else {
                    this.cronTasks[cronTaskIndex] = message.data.new_val;
                }
            }
        }
    }

    fetchCronTaskList() {
        this.cronTaskService.getTaskList().subscribe(res => {
            this.cronTasks = res['data'];
        });
    }

    ngOnInit() {
    }

}
