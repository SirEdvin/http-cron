import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CronManagementComponent } from './cron-management.component';

describe('CronManagementComponent', () => {
  let component: CronManagementComponent;
  let fixture: ComponentFixture<CronManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CronManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CronManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
