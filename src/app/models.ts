export interface HTTPCronTask {
    url: string;
    name: string;
    cron_expression: string;
    worker_id: string;
    status: string;
    last_execution_time: string;
    next_execution_time: string;
    description: string;
    is_active: boolean;
    id: string;
}