import { TestBed, inject } from '@angular/core/testing';

import { CronTaskService } from './cron-task.service';

describe('CronTaskService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CronTaskService]
    });
  });

  it('should be created', inject([CronTaskService], (service: CronTaskService) => {
    expect(service).toBeTruthy();
  }));
});
